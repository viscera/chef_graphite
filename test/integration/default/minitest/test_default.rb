require 'minitest/spec'
require 'minitest/autorun'

describe 'graphite::default' do

  describe 'runit graphite daemons' do
    it 'runit graphite' do
      assert(File.exist?('/etc/sv/graphite'),
             '/etc/sv/graphite is missing.')
    end

    it 'runit carbon-cache' do
      assert(File.exist?('/etc/sv/carbon-cache'),
             '/etc/sv/carbon-cache is missing.')
      assert(File.read('/etc/sv/carbon-cache/run')
             .match(%|exec chpst -u graphite -l /opt/graphite/storage/carbon-cache.lock -- /opt/graphite/bin/carbon-cache.py --debug start|),
            '/etc/sv/carbon-cache/run does not contain the expected content')
    end
  end
end
