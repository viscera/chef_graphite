require 'minitest/autorun'
require_relative '../../libraries/carbon_config'

require 'chef'

class TestCarbonConfig < Minitest::Test
  TEST_CONFIG = {
    z_last_section: {
      first: 'string',
      second: 1234,
      third: '"/regexp/"'
    },
    first_section: {
      first: 'string',
      second: 1234,
      third: '"/regexp/"'
    },
    second_section: {
      'CAPITALIZED' => 'string',
      retentions: '15s:7d,1m:21d,15m:5y',
      pattern: '^servers\.www.*\.workers\.busyWorkers$'
    }
  }

  INI_RESULT =
    '
[first_section]
first = string
second = 1234
third = "/regexp/"

[second_section]
CAPITALIZED = string
retentions = 15s:7d,1m:21d,15m:5y
pattern = ^servers\.www.*\.workers\.busyWorkers$

[z_last_section]
first = string
second = 1234
third = "/regexp/"
'

  def setup
    @config = TEST_CONFIG
    @rendered_text = ChefGraphite::Config.hash_to_ini(@config)
  end

  def test_render
    assert_equal(INI_RESULT, @rendered_text)
  end
end

class TestCarbonConfigYAML < Minitest::Test
  TEST_CONFIG = Chef::Node::ImmutableMash.new({
    :name  => 'server1',
    :items => Chef::Node::ImmutableArray.new(['sword', 'shield', 'letter from mom']),
    :env   => Chef::Node::ImmutableMash.new({
      'DATABASE_URL' => 'foo'
    })
  })

  YAML_RESULT =
'---
name: server1
items:
- sword
- shield
- letter from mom
env:
  DATABASE_URL: foo
'

  def test_render
    assert_equal(ChefGraphite::Config.hash_to_yaml(TEST_CONFIG), YAML_RESULT)
  end
end

class TestArrayToLine < Minitest::Test
  TEST_CONFIG = ['a', 'b', 'c']
  RESULT =
'a
b
c'

  def test_render
    assert_equal(ChefGraphite::Config.array_to_text(TEST_CONFIG), RESULT)
  end
end
