default[:graphite][:virtualenv] = '/opt/graphite/.venv/'
default[:graphite][:app_dir] = '/opt/graphite/webapp/graphite/'
default[:graphite][:conf_dir] = '/opt/graphite/conf/'
default[:graphite][:data_dir] = '/opt/graphite/storage/'
default[:graphite][:home_dir] = '/opt/graphite/share/graphite/'
default[:graphite][:log_dir] = '/opt/graphite/log/'
default[:graphite][:pid_dir] = '/var/run/graphite'
default[:graphite][:webapp_dir] = '/opt/graphite/webapp/'
default[:graphite][:user] = 'graphite'
default[:graphite][:group] = 'graphite'
default[:graphite][:user_home] = '/opt/graphite/'
default[:graphite][:carbon][:user] = :graphite
default[:graphite][:carbon][:lock_dir] = '/opt/graphite/storage/'
default[:graphite][:graphite_web][:user] = :graphite
default[:graphite][:graphite_web][:version] = '0.9.12'
default[:graphite][:cairo][:source][:url] =
  'http://cairographics.org/releases/'
default[:graphite][:cairo][:source][:version] = '1.10.2'
default[:graphite][:carbon][:version] = '0.9.12'
default[:graphite][:db][:admin_email] = 'graphite@example.com'
default[:graphite][:db][:admin_password] =
  'sha1$aaab5$56123f60bc5dcb3b60076928f868ab881245f1fc'
default[:graphite][:db][:admin_user] = :graphite
default[:graphite][:whisper][:user] = :graphite
default[:graphite][:whisper][:version] = '0.9.12'
default[:graphite][:api][:version] = '1.0.1'
default[:graphite][:api][:config_path] = '/etc/graphite-api.yaml'

default[:graphite][:packages] = {
  'gunicorn' => '18.0',
  'django' => '1.4.11',
  'django-tagging' => '0.3.2',
  'simplejson' => '3.4.0',
  'Twisted' => '11.1.0',
  'python-memcached' => '1.53',
  'pyparsing' => '2.0.2',
  'txAMQP' => '0.6.2',
  'pytz' => '2014.2'
}

default[:graphite][:carbon][:config] = {
  cache: {
    MAX_CACHE_SIZE: 'inf',
    MAX_UPDATES_PER_SECOND: 1000,
    MAX_CREATES_PER_MINUTE: 50,
    LINE_RECEIVER_INTERFACE: '0.0.0.0',
    LINE_RECEIVER_PORT: 2003,
    ENABLE_UDP_LISTENER: 'False',
    UDP_RECEIVER_INTERFACE: '0.0.0.0',
    UDP_RECEIVER_PORT: 2003,
    PICKLE_RECEIVER_INTERFACE: '0.0.0.0',
    PICKLE_RECEIVER_PORT: 2004,
    USE_INSECURE_UNPICKLER: 'False',
    CACHE_QUERY_INTERFACE: '0.0.0.0',
    CACHE_QUERY_PORT: 7002,
    USE_FLOW_CONTROL: 'True',
    LOG_UPDATES: 'False',
    WHISPER_AUTOFLUSH: 'False'
  },
  relay: {
    LINE_RECEIVER_INTERFACE: '0.0.0.0',
    LINE_RECEIVER_PORT: 2013,
    PICKLE_RECEIVER_INTERFACE: '0.0.0.0',
    PICKLE_RECEIVER_PORT: 2014,
    RELAY_METHOD: 'rules',
    REPLICATION_FACTOR: 1,
    DESTINATIONS: '127.0.0.1:2004',
    MAX_DATAPOINTS_PER_MESSAGE: 500,
    MAX_QUEUE_SIZE: 10_000,
    USE_FLOW_CONTROL: 'True'
  },
  aggregator: {
    LINE_RECEIVER_INTERFACE: '0.0.0.0',
    LINE_RECEIVER_PORT: 2023,
    PICKLE_RECEIVER_INTERFACE: '0.0.0.0',
    PICKLE_RECEIVER_PORT: 2024,
    DESTINATIONS: '127.0.0.1:2004',
    REPLICATION_FACTOR: 1,
    MAX_QUEUE_SIZE: 10_000,
    USE_FLOW_CONTROL: 'True',
    MAX_DATAPOINTS_PER_MESSAGE: 500,
    MAX_AGGREGATION_INTERVALS: 5
  }
}

default[:graphite][:api][:config] = {
  'time_zone' => 'UTC'
}

default[:graphite][:carbon][:storage][:schema] = {
  wildcard: {
    pattern: '.*',
    retentions: '60s:1d'
  }
}

default[:graphite][:carbon][:storage][:aggregation] = {}

default[:graphite][:carbon][:cache][:blacklist] = []
default[:graphite][:carbon][:cache][:whitelist] = []
