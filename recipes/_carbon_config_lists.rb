file "#{node[:graphite][:conf_dir]}/whitelist.conf" do
  owner node[:graphite][:user]
  group node[:graphite][:group]
  mode 00644
  content lazy {
    ChefGraphite::Config.array_to_text(
      node[:graphite][:carbon][:cache][:whitelist]
    )
  }
end

file "#{node[:graphite][:conf_dir]}/blacklist.conf" do
  owner node[:graphite][:user]
  group node[:graphite][:group]
  mode 00644
  content lazy {
    ChefGraphite::Config.array_to_text(
      node[:graphite][:carbon][:cache][:blacklist]
    )
  }
end
