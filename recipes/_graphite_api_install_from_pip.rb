python_pip 'graphite-api' do
  version node[:graphite][:api][:version]
  virtualenv node[:graphite][:virtualenv]
  action :install
end
