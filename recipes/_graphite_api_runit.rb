include_recipe 'runit'

runit_service 'graphite-api' do
  env({
    'GRAPHITE_API_CONFIG' => node[:graphite][:api][:config_path]
  })
end
